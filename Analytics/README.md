# Unity Erogames Analytics Plugin

## Requirements
Unity version `5.6.4` or higher.

## Get started

### How to install
1. Add the following line as dependency to Packages/manifest.json:
```json
"com.erogames.auth": "https://gitlab.com/sky-network/erogames-unity-sdk.git#analytics-v<x.y.z>-upm"
```
2. Add the Game Package Registry by Google to Packages/manifest.json:
```json
"scopedRegistries": [
  {
    "name": "Game Package Registry by Google",
    "url": "https://unityregistry-pa.googleapis.com",
    "scopes": [
      "com.google"
    ]
  }
]
```
3. [Optionally] In case Unity Package Manager is unavailable just import `erogames-analytics-v<x.y.z>.unitypackage` into a project.
4. Go to (in Unity Editor) `Assets > External Dependency Manager > Android Resolver` and click `Resolve` (in some cases `Force Resolve`).

### How to make Android build on Unity with Gradle version less than 5.1.1 (see [Gradle for Android](https://docs.unity3d.com/Manual/android-gradle-overview.html))
1. Export project (`File > Build Settings > Export Project`).
2. Open the project in Android Studio.
3. Update Android Gradle Plugin version to 3.4.0 or higher. In the project-level `build.gradle`:
```groovy
dependencies {
  ...
  classpath 'com.android.tools.build:gradle:3.4.0' // or higher
  ...
}
```
4. Add Java 8 language features compatibility. In the project-level `build.gradle` (or in the app-level `build.gradle` if exists):
```groovy
android {
  ...
  compileOptions {
      sourceCompatibility JavaVersion.VERSION_1_8
      targetCompatibility JavaVersion.VERSION_1_8
  }
  ...
}
```

### Log event
```csharp
ErogamesAnalytics.LogEvent("install");
```
Log event with a payload:
```csharp
Dictionary<string, string> data = new Dictionary<string, string>()
{
  ["app_id"] = "some_app_id",
  ["source_Id"] = "some_source_id",
  ["one_more_key"] = "one_more_value"
};
ErogamesAnalytics.LogEvent("install", data);
```

### How to debug on Android:
```shell
adb logcat -s EventWorker -s TrackProviderUtil
```
