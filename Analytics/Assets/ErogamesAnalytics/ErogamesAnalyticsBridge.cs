using System.Collections.Generic;
using ErogamesAnalyticsNS.Util;
using UnityEngine;

namespace ErogamesAnalyticsNS
{
    internal sealed class ErogamesAnalyticsBridge : Singleton<ErogamesAnalyticsBridge>
    {
        private const string BridgeClassName = "com.erogames.analytics.unitywrapper.ErogamesAnalyticsBridge";
        private AndroidJavaClass bridgeJavaClass;

        private AndroidJavaClass GetBridgeJavaClass()
        {
            if (bridgeJavaClass == null)
            {
                bridgeJavaClass = new AndroidJavaClass(BridgeClassName);
            }
            return bridgeJavaClass;
        }

        public void LogEvent(string eroEvent, Dictionary<string, string> eroParams)
        {
            GetBridgeJavaClass().CallStatic("logEvent", eroEvent, Utils.DictionaryToJson(eroParams));
        }
    }
}
