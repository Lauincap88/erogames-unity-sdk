﻿using System;
using System.Collections;
using System.Collections.Generic;
using ErogamesAnalyticsNS.Model;
using ErogamesAnalyticsNS.Util;
using UnityEngine;
using UnityEngine.Networking;

namespace ErogamesAnalyticsNS.Api
{
    internal class ApiService : Singleton<ApiService>
    {
        private const string BaseUrl = "https://erogames.com/tracking/goal/";
        private const string EventEndPoint = BaseUrl + "{0}/{1}/{2}";
        private const string PostEventEndPoint = BaseUrl;

        /// <summary>
        /// Send Event.
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="eventId"></param>
        /// <param name="trackId"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator SendEvent(string appId, string eventId, string trackId, Action<Result<bool>> onResult)
        {
            string url = string.Format(EventEndPoint, appId, eventId, trackId);
            using (UnityWebRequest unityWebRequest = UnityWebRequest.Get(url))
            {
#if UNITY_5_6 || UNITY_2017_1
				yield return unityWebRequest.Send();
#else
                yield return unityWebRequest.SendWebRequest();
#endif
#if UNITY_2017_1_OR_NEWER
                if (!unityWebRequest.isNetworkError && !unityWebRequest.isHttpError)
#else
				if (!unityWebRequest.isError && Utils.IsResponseSuccessful(unityWebRequest))
#endif
                {
                    EventResp eventResp = JsonUtility.FromJson<EventResp>(unityWebRequest.downloadHandler.text);
                    if (eventResp.IsSuccessfull())
                        onResult.Invoke(Result<bool>.Success(true));
                    else
                        onResult.Invoke(Result<bool>.Error(eventResp.status));
                }
                else
                {
                    onResult.Invoke(Result<bool>.Error(ExtractErrorMessage(unityWebRequest.downloadHandler.text)));
                }
            }
        }

        /// <summary>
        /// Send Event.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        internal IEnumerator SendToken(Dictionary<string, string> data, Action<Result<bool>> onResult)
        {
            WWWForm form = new WWWForm();
            foreach (KeyValuePair<string, string> entry in data)
            {
                form.AddField(entry.Key, entry.Value);
            }

            using (UnityWebRequest unityWebRequest = UnityWebRequest.Post(PostEventEndPoint, form))
            {
#if UNITY_5_6 || UNITY_2017_1
				yield return unityWebRequest.Send();
#else
                yield return unityWebRequest.SendWebRequest();
#endif
#if UNITY_2017_1_OR_NEWER
                if (!unityWebRequest.isNetworkError && !unityWebRequest.isHttpError)
#else
				if (!unityWebRequest.isError && Utils.IsResponseSuccessful(unityWebRequest))
#endif
                {
                    EventResp eventResp = JsonUtility.FromJson<EventResp>(unityWebRequest.downloadHandler.text);
                    if (eventResp.IsSuccessfull())
                        onResult.Invoke(Result<bool>.Success(true));
                    else
                        onResult.Invoke(Result<bool>.Error(eventResp.status));
                }
                else
                {
                    onResult.Invoke(Result<bool>.Error(ExtractErrorMessage(unityWebRequest.downloadHandler.text)));
                }
            }
        }


        /// <summary>
        /// Extract error message from response.
        /// </summary>
        /// <param name="rawResponse"></param>
        /// <returns></returns>
        private string ExtractErrorMessage(string rawResponse)
        {
            ErrorResp errorResp;
            try
            {
                errorResp = JsonUtility.FromJson<ErrorResp>(rawResponse);
            }
            catch (Exception e)
            {
                return e.Message;
            }

            if (errorResp == null) return rawResponse;
            if (!string.IsNullOrEmpty(errorResp.message)) return errorResp.message;
            if (errorResp.errors != null && errorResp.errors.Count > 0) return string.Join(", ", errorResp.errors.ToArray());
            if (!string.IsNullOrEmpty(errorResp.error_description)) return errorResp.error_description;

            return rawResponse;
        }
    }
}
