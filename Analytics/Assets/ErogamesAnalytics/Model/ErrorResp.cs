﻿using System;
using System.Collections.Generic;

namespace ErogamesAnalyticsNS.Model
{
    [Serializable]
    public class ErrorResp
    {
        public string message;
        public List<string> errors;
        public string error_description;
    }
}
