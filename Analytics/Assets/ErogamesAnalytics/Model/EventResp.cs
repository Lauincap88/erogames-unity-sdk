﻿using System;

namespace ErogamesAnalyticsNS.Model
{
    [Serializable]
    internal class EventResp
    {
        public string status;

        internal bool IsSuccessfull()
        {
            return "ok".Equals(status, StringComparison.OrdinalIgnoreCase);
        }
    }
}
