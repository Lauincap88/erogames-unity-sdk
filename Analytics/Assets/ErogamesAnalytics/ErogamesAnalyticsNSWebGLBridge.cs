﻿using System.Collections;
using System.Collections.Generic;
using ErogamesAnalyticsNS.Api;
using ErogamesAnalyticsNS.Model;
using ErogamesAnalyticsNS.Util;
using UnityEngine;
using System.Runtime.InteropServices;
using System;

namespace ErogamesAnalyticsNS
{
    internal sealed class ErogamesAnalyticsNSWebGLBridge : Singleton<ErogamesAnalyticsNSWebGLBridge>
    {

#if UNITY_WEBGL
        [DllImport("__Internal")]
        private static extern void GetOrCreateTrackId(string defaultUuid);
#endif

        private Dictionary<string, string> data;

        internal void LogEvent(string eventId, Dictionary<string, string> data)
        {
            this.data = new Dictionary<string, string>(data);
            this.data["event_id"] = eventId;
            this.data["platform"] = Application.platform.ToString();

            if (!this.data.ContainsKey("app_id") || string.IsNullOrEmpty(this.data["app_id"]))
                this.data["app_id"] = Utils.ParseQueryString(Application.absoluteURL, "ea_app_id", "unknown");

            if (!this.data.ContainsKey("source_id") || string.IsNullOrEmpty(this.data["source_id"]))
                this.data["source_id"] = Utils.ParseQueryString(Application.absoluteURL, "ea_source_id", "unknown");

            if (!this.data.ContainsKey("track_id") || string.IsNullOrEmpty(this.data["track_id"]))
                this.data["track_id"] = Utils.ParseQueryString(Application.absoluteURL, "ea_track_id", null);

            if (!string.IsNullOrEmpty(this.data["track_id"]))
            {
                StartCoroutine(CompleteLogEvent());
            }
            else
            {
#if UNITY_WEBGL
                GetOrCreateTrackId(Guid.NewGuid().ToString());
#endif
            }
        }

        internal void OnGetOrCreateTrackId(string trackId)
        {
            data["track_id"] = trackId;
            StartCoroutine(CompleteLogEvent());
        }

        private IEnumerator CompleteLogEvent()
        {
            Debug.Log("Send event data: " + Utils.DictionaryToJson(data));

            yield return ApiService.Instance.SendEvent(data["app_id"], data["event_id"], data["track_id"], (result) =>
            {
                string status = (result is Result<bool>.SuccessResult) ? "SUCCESS" : "FAILURE";
                Debug.Log("Send event status: " + status);
                if (!(result is Result<bool>.SuccessResult))
                    Debug.LogError("Send event error: " + result.ErrorMsg);
            });

            data = null;
        }
    }
}
