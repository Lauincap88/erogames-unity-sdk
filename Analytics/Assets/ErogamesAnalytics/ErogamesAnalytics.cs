using System.Collections.Generic;
using UnityEngine;

namespace ErogamesAnalyticsNS
{
    public class ErogamesAnalytics : MonoBehaviour
    {
        private ErogamesAnalytics() { }

        /// <summary>
        /// Send an event to the server asynchronously.
        /// The event will be added to the queue immediately.
        /// But sending may be delayed if, for example, there is no internet connection, etc.
        /// A user does not have to worry about it because the event will be stored until it is sent.
        ///
        /// See also: https://gitlab.com/sky-network/erogames-android-sdk/-/tree/master/analytics
        /// </summary>
        /// <param name="eroEvent">Event name</param>
        public static void LogEvent(string eventId)
        {
            LogEvent(eventId, new Dictionary<string, string>());
        }

        /// <summary>
        /// Send an event to the server asynchronously.
        /// The event will be added to the queue immediately.
        /// But sending may be delayed if, for example, there is no internet connection, etc.
        /// A user does not have to worry about it because the event will be stored until it is sent.
        ///
        /// See also: https://gitlab.com/sky-network/erogames-android-sdk/-/tree/master/analytics
        /// </summary>
        /// <param name="eroEvent">Event name</param>
        public static void LogEvent(string eventId, Dictionary<string, string> data)
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    ErogamesAnalyticsBridge.Instance.LogEvent(eventId, data);
                    return;
                case RuntimePlatform.WebGLPlayer:
                    ErogamesAnalyticsNSWebGLBridge.Instance.LogEvent(eventId, data);
                    return;
            }

            Debug.LogError(Application.platform.ToString() + " is not supported.");
        }
    }
}
