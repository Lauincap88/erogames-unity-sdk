﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

namespace ErogamesAnalyticsNS.Util
{
    internal class Utils
    {
        internal static string ParseQueryString(string url, string key)
        {
            return ParseQueryString(url, key, null);
        }

        internal static string ParseQueryString(string url, string key, string defaultValue)
        {
            Uri appUri;
            string returnValue = defaultValue;
            try
            {
                appUri = new Uri(url);
            }
            catch (UriFormatException e)
            {
                Debug.LogError(e);
                return returnValue;
            }

            string query = appUri.Query;
            query = query.Replace("?", "");
            if (query == null || query.Length < 1 || !query.Contains("=")) return returnValue;
            char equalChar = "=".ToCharArray()[0];
            char askChar = "&".ToCharArray()[0];

            Dictionary<string, string> keysValues = query.Split(askChar).ToDictionary(
                c => c.Split(equalChar)[0],
                c => Uri.UnescapeDataString(c.Split(equalChar)[1])
            );

            return keysValues.ContainsKey(key) ? keysValues[key] : returnValue;
        }

        internal static string DictionaryToJson(Dictionary<string, string> dict)
        {
            if (dict == null) return "{}";
            List<string> keyValueList = new List<string>();
            foreach (KeyValuePair<string, string> entry in dict)
            {
                keyValueList.Add(string.Format("\"{0}\": \"{1}\"", entry.Key, entry.Value));
            }
            return "{" + string.Join(",", keyValueList.ToArray()) + "}";
        }

        internal static bool IsResponseSuccessful(UnityWebRequest unityWebRequest)
        {
            return unityWebRequest.responseCode >= 200 && unityWebRequest.responseCode <= 299;
        }
    }
}
