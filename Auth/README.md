# Unity Erogames Auth Plugin

The plugin for user authentication via Erogames API.

## Requirements
Unity version `5.6.4` or higher.

## Get started

### How to install
1. Add the following line as dependency to Packages/manifest.json:
```json
"com.erogames.auth": "https://gitlab.com/sky-network/erogames-unity-sdk.git#auth-v<x.y.z>-upm"
```
2. Add the Game Package Registry by Google to Packages/manifest.json:
```json
"scopedRegistries": [
  {
    "name": "Game Package Registry by Google",
    "url": "https://unityregistry-pa.googleapis.com",
    "scopes": [
      "com.google"
    ]
  }
]
```
3. [Optionally] In case Unity Package Manager is unavailable just import `erogames-auth-v<x.y.z>.unitypackage` into a project.
4. Go to (in Unity Editor) `Assets > External Dependency Manager > Android Resolver` and click `Resolve` (in some cases `Force Resolve`).
5. Add your `client id`. In Unity Editor: `Window > ErogamesAuth > Client ID`.

### How to make Android build on Unity with Gradle version less than 5.1.1 (see [Gradle for Android](https://docs.unity3d.com/Manual/android-gradle-overview.html))
1. Export project (`File > Build Settings > Export Project`).
2. Open the project in Android Studio.
3. Update Android Gradle Plugin version to 3.4.0 or higher. In the project-level `build.gradle`:
```groovy
dependencies {
  ...
  classpath 'com.android.tools.build:gradle:3.4.0' // or higher
  ...
}
```
4. Add Java 8 language features compatibility. In the project-level `build.gradle` (or in the app-level `build.gradle` if exists):
```groovy
android {
  ...
  compileOptions {
      sourceCompatibility JavaVersion.VERSION_1_8
      targetCompatibility JavaVersion.VERSION_1_8
  }
  ...
}
```

## Important!
The login process starts automatically once the user launches the app.  
Use a callback to check if a user is logged in.
```csharp
ErogamesAuth.OnAuth(callback);
```
Since an app page may reload during authentication it's recommended to start the initialization of an app (loading of some data, etc.) after a successful login.

### Whitelabel info.
All necessary data such as base white-label site URL, logo URL, "buy Erogold" URL, etc.) can be taken as follows:
```csharp
ErogamesAuth.GetWhitelabel();
```
If a user is not logged in and there is a need for the above data, a whitelabel info should be loaded first:
```csharp
ErogamesAuth.LoadWhitelabel(onSuccess, onFailure);
```

### Log in (available for Android and WebGL only):
```csharp
ErogamesAuth.Login("en"); // "en" is a language code
```

### Log in by username(email)/password (available for Mac/PC only):
```csharp
ErogamesAuth.LoginByPassword(username, password);
```

### Sign up (available for Android and WebGL only):
```csharp
ErogamesAuth.Signup("fr");
```

### Register of a new user (available for Mac/PC only):
```csharp
ErogamesAuth.RegisterUser(username, password, email, checkTermsOfUse, onSuccess, onFailure);
```

### Log out case 1:
```csharp
// The local data only will be cleaned
ErogamesAuth.Logout();
```

### Log out case 2:
```csharp
// A user will log out of the web site and
// the local data will be cleaned as well
ErogamesAuth.Logout(true);
```

### Get current user:
```csharp
ErogamesAuth.GetUser();
```

### Reload user:
```csharp
ErogamesAuth.ReloadUser(onSuccess, onFailure);
```

### Get token:
```csharp
ErogamesAuth.GetToken();
```

### Refresh token:
```csharp
ErogamesAuth.RefreshToken(onSuccess, onFailure);
```

### Create/update user data points:
```csharp
ErogamesAuth.AddDataPoints(dataPoints, onSuccess, onFailure);
```

### Load current quest info:
```csharp
ErogamesAuth.LoadCurrentQuest(onSuccess, onFailure);
```
