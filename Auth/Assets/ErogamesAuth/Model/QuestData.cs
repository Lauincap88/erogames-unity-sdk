﻿using System;
using System.Collections.Generic;

namespace ErogamesAuthNS.Model
{
    [Serializable]
    public class QuestData
    {
        public Quest quest;
        public List<Leader> leaders;
        public UserClanAttempt user_clan_attempt;
        public UserScore user_score;
    }


    [Serializable]
    public class Quest
    {
        public int id;
        public string title;
        public string description;
        public string cover_url;
        public string finished_at;
    }


    [Serializable]
    public class Leader
    {
        public int position;
        public string name;
        public string cover_picture_url;
        public string clan_leader_name;
        public long score;
    }


    [Serializable]
    public class UserClanAttempt
    {
        public int position;
        public string name;
        public string cover_picture_url;
        public string clan_leader_name;
        public int score;
    }


    [Serializable]
    public class UserScore
    {
        public int total;
        public List<EarnedPoint> earned_points;
    }


    [Serializable]
    public class EarnedPoint
    {
        public int points;
        public string title;
    }
}
