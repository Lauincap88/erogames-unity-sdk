﻿using System;

namespace ErogamesAuthNS.Model
{
    [Serializable]
    public class DataPoint
    {
        public string field;
        public string value;

        public DataPoint(string field, string value)
        {
            this.field = field;
            this.value = value;
        }
    }
}
