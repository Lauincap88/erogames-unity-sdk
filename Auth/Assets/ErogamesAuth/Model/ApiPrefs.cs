﻿using System;

namespace ErogamesAuthNS.Model
{
    [Serializable]
    public class ApiPrefs
    {
        public string whitelabelId;
        public string baseUrl;
        public string clientId;
        public bool autoLogin = true;
        public string clientSecret;
        public string accessKey;
    }
}
