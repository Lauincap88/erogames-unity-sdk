﻿using System;

namespace ErogamesAuthNS.Model
{
    [Serializable]
    public class Token
    {
        public string access_token;
        public string refresh_token;
        public string token_type;
        public int expires_in;
        public int created_at;
    }
}
