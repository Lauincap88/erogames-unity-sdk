﻿using System;
using ErogamesAuthNS.Model;
using UnityEngine;

namespace ErogamesAuthNS.Util
{
    internal class AuthUtil
    {
        internal static readonly string ErogoldUrlFormat = "{0}/{1}/buy-erogold";
        internal static readonly string WebLogoutUrlFormat = "{0}/logout?token={1}&redirect_uri={2}";
        internal static readonly string AuthUrlFormat = "{0}{1}" +
            "redirect_uri={2}" +
            "&client_id={3}" +
            "&locale={4}" +
            "&force_registration={5}" +
            "&response_type=code" +
            "&disclaimer=none";

        private AuthUtil() { }

        internal static string BuildAuthUrl(
            string authorizeUrl,
            string redirectUri,
            string clientId,
            string lang,
            bool forceRegistration
            )
        {
            return string.Format(AuthUrlFormat,
             authorizeUrl,
             authorizeUrl.Contains("?") ? "&" : "?",
             redirectUri,
             clientId,
             lang,
             forceRegistration.ToString());
        }

        internal static string BuildRedirectUri()
        {
            return BuildRedirectUri(false);
        }

        internal static string BuildRedirectUri(bool encode)
        {
            string url = Application.absoluteURL;
            url = Utils.RemoveQueryString(url, "code");

            if (encode)
            {
                return Uri.EscapeDataString(url);
            }
            Debug.Log("Redirect URL: " + url);
            return url;
        }

        internal static string BuildWebLogoutUrl(string baseUrl, string token, string redirectUri)
        {
            return string.Format(WebLogoutUrlFormat, baseUrl, token, redirectUri);
        }
    }
}
